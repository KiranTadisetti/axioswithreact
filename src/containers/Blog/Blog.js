import React, { Component } from 'react';
import { Route, NavLink , Switch} from 'react-router-dom';

import './Blog.css';
import Posts from '../Posts/Posts';
import NewPost from './NewPost/NewPost';
import FullPost from './FullPost/FullPost';

class Blog extends Component {
    render() {

        return (
            <div>
                <header className="Blog">
                    <nav>
                        <ul>
                            <li>
                                <NavLink
                                    to="/"
                                    exact>
                                    Home
                                </NavLink>
                            </li>
                            <li> <NavLink to={{
                                pathname: "/newpost",
                                hash: '#submit',
                                search: "?quick-submit=true"
                            }} exact>New Post</NavLink> </li>
                        </ul>
                    </nav>
                </header>
                <Switch>
                <Route path="/" exact component={Posts} />
                <Route path="/newpost" exact component={NewPost} />
                <Route path="/:id" exact component={FullPost} />
                </Switch>

            </div>
        );
    }
}

export default Blog;