import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import axios from '../../axios'

import Post from '../../components/Post/Post';
import './Posts.css'

class Posts extends Component{
    state = {
        posts: [],
        selectedPostId: undefined,
        error: false
    }
    componentDidMount() {
        console.log(this.props)
        axios.get("/posts")
            .then(response => {
                const posts = response.data.slice(0, 4);
                const updatedPosts = posts.map(post => {
                    return {
                        ...post,
                        author: 'kiran'
                    }
                })
                console.log(updatedPosts)
                this.setState({ posts: updatedPosts })
                this.setState({ error:false });
            })
            .catch(error => {
                //alert(error)
                console.log(error)
                this.setState({ error:true });
            })
    }
    postsSelectedHandler = (id) => {
        this.setState({ selectedPostId: id });
    }
    render(){
        let posts = <p style={{ textAlign: 'center', color: 'red' }}> <strong>Something went wrong!!</strong></p>
        if (!this.state.error) {
            posts = this.state.posts.map((post) => {
                return (<Link to={"/"+post.id} key={post.id}>
                    <Post
                        title={post.title}
                        author={post.author}
                        clicked={() => this.postsSelectedHandler(post.id)} />
                </Link>
                )
            })
        }
        return(
        <div>
            <section className="Posts">
                    {posts}
                </section>
        </div>
        )
    }
}

export default Posts;